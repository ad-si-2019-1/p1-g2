## P1 - Sistemas Distribuídos 2019/01

#### Membros:


- [Vitor de Lima](@vitorlc) *- Líder/Desenvolvedor*
- [Tiago Hermano](@tiagohermano) *- Desenvolvedor*
- [Rodrigo Araújo](@rodrigowar13) *- Desenvolvedor*

# Saiba Mais
**Acesse a documentação completa na [Wiki](https://gitlab.com/ad-si-2019-1/p1-g2/wikis/home).**
